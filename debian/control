Source: stockpile-clojure
Section: java
Priority: optional
Maintainer: Debian Clojure Maintainers <team+clojure@tracker.debian.org>
Uploaders: Apollon Oikonomopoulos <apoikos@debian.org>
Build-Depends:
 debhelper (>= 10),
 javahelper (>= 0.32),
 maven-repo-helper (>= 1.7),
 clojure (>= 1.8),
 libcommons-lang3-java (>= 3.4),
 libpuppetlabs-i18n-clojure (>= 0.4.3),
 libtext-markdown-perl | markdown,
 default-jdk-headless
Standards-Version: 4.0.0
Vcs-Git: https://salsa.debian.org/clojure-team/stockpile-clojure.git
Vcs-Browser: https://salsa.debian.org/clojure-team/stockpile-clojure
Homepage: https://github.com/puppetlabs/stockpile

Package: libstockpile-clojure
Architecture: all
Depends:
 ${java:Depends},
 ${misc:Depends}
Recommends: ${java:Recommends}
Description: Simple, durable Java queuing library
 A simple, durable Java queueing library. Stockpile supports the durable
 storage and retrieval of data. After storage, stockpile returns an "entry"
 that can be used to access the data later, and when no longer needed, the data
 can be atomically discarded.
